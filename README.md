# OpenMOF 

This repo provides a plain HTML group page for the OpenMOF project. 

[OpenMOF GitLab page](https://openmof.gitlab.io/openmof)

# Infos 

- HTML color picker 
    + https://htmlcolorcodes.com/

- public/index.html
    + main file 
    + plain html 
    + all-in-one; all pages are in one html document 

- public/style.css 
    + style definitions for index.html 
 
